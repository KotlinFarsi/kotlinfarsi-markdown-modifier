---
layout: tutorial
title: "همکاری با JavaScript"
category: js
permalink: /tutorials/js/javascript-introp/
editlink: https://github.com/KotlinFarsi/OpenSourceTutorials-Browser/edit/master/src/javascript-introp/README.md
---


<div dir="rtl" markdown="1">



یکی از ویژگی­های کاتلین همکاری مستقیم با فایل JavaScript هه.حالا ما چرا باید بخوایم که با جاوا اسکریپت همکاری کنیم؟ زمانی پیش­خواهد اومد که ما بخوایم به صورت مستقیم با فایل­های جاوا اسکریپتیمون ارتباط برقرار کنیم، بخوایم مقدار متغیری رو بخونیم، بخوایم یک تابعی رو ازش صدا بزنیم یا این که اصلا بخوایم با یک کتابخونه که توی جاوااسکریپته استفاده کنیم. توی اینجاها خیلی مهمه که بتونیم مستقیم از فایل­های جاوااسکریپتیمون استفاده کنیم 

توی این قسمت می­خواییم یکسری اشاره­هایی به بحث همکاری با جاوا اسکریپت بکنیم.

</div>
